# README #

### IMPORTANTE ANTES DE CORREGIR ###
* la carpeta 'tareacalificada/' consta de dos partes: 
    1) '--LECCION 4--/' :: la cual dentro de la subcarpeta '/leccion4' tiene una pagina web muy similar a la hecha por el profesor Lamonica.
    2) 'tareaCalif ' :: esta contiene codigo propio, luego de todas las clases, pero sin seguir ningun tutorial, 
esta division se debe a que no entendi si la consigna pide realizar otro trabajo desde cero o uno similar al de los tutoriales.
La parte 1) tiene frotend mas desarrollado || mientras que en la parte 2) solo me ocupe de generar una API que permita modificar la informacion, esta informacion se puede ver en '/'

### How do I get set up? ###
* 'https://bitbucket.org/lisandrogq7/tareacalificada/'-- clone repository

    1) '--LECCION 4--
        * 'npm install' -- para instalar las dependencias necesarias //recordar ejecutar este comando en '/tareaCalif'
        * 'npm start' -- para iniciar el servidor en el puerto 4000
        * la forma de uso de esta pagina es identica a la del ejemplo del prof. Lamonica
    
    2) '--tareaCalif--
        * 'npm install' -- para instalar las dependencias necesarias //recordar ejecutar este comando en '/--LECCION 4--/leccion4'
        * 'npm start' -- para iniciar el servidor en el puerto 3000
        * 'http://localhost:4000/api/marcadores/ACCION'-- reemplazar accion por las distintas rutas especificadas ne /routes/api/routesAPI.js para testear el funcionamiento(en postman, con el metodo http correcto)
        * 'http://localhost:3000/' -- para ver los marcadores en el mapa, aqui tambien se pueden ver los cambios que se hagan con el punto anterior, se debe refrescar la pestaña de Google para hacerlo.


### Por favor ser claro en la correcion, es mi primera vez en un curso ###