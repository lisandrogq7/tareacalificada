const express  = require('express');
const router = express.Router();
const Marcadores= require('../../models/marcadores');
const MarcadorController= require('../../controllers/marcadorControllerAPI');

router.get('/',MarcadorController.readMarcadores); // http://localhost:3000/api/marcadores ==> metodo GET
router.post('/create',MarcadorController.createMarcador);// http://localhost:3000/api/marcadores/create ==> metodo POST
router.put('/:id/update',MarcadorController.updateMarcador);// http://localhost:3000/api/marcadores/id/update ==> metodo PUT
router.delete('/:id/delete',MarcadorController.deleteMarcador);// http://localhost:3000/api/marcadores/11/delete =>> metodo DELETE
//___Todas estas modificaciones se pueden ver en http://localhost:3000/ con solo recargar la pagina___



module.exports= router;