var mymap = L.map('mapid').setView([-34.5949836,-58.4054631], 15);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    
}).addTo(mymap);

//var marker = L.marker([-34.5949836,-58.4054631],{title:"oal"}).addTo(mymap);
//var marker = L.marker([-34.595577,-58.4018779],{title:"oal2"}).addTo(mymap);

//esto mostrara las marcas en el mapa
$.ajax({
    dataType: 'json',
    url : 'api/marcadores',
    success: function(result){
        console.log(result.marcadores);
        result.marcadores.forEach(function(marcador){
            console.log(marcador.ubicacion, marcador.id)
            L.marker(marcador.ubicacion,{title:marcador.descripcion}).addTo(mymap);
        })
    }
})