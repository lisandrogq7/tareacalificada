const Marcador = require('../models/marcadores');



exports.readMarcadores=function(req,res){
    res.status(200).json({marcadores:Marcador.allMarcs});
}

exports.createMarcador =function(req,res){
    var marcador= new Marcador(req.body.id,req.body.descripcion,req.body.ubicacion);
    Marcador.addMarc(marcador);

    res.status(200).json({message:'todo ok'});
}

exports.updateMarcador= function(req,res){
    
    var marcador = new Marcador.findMarc(req.params.id);
    if(marcador.message!='No hay un marcador de id '+ req.params.id){ // esto se podria hacer con un bool, pero lo dejo asi :DD

    console.log(req.params.id);
    marcador.id= req.body.id;
    marcador.descripcion= req.body.descripcion;
    marcador.ubicacion= req.body.ubicacion;
    
    res.status(200).json(marcador);
}
else res.status(400).json({message:marcador.message})
}

exports.deleteMarcador= function(req,res){
    
    for(i=0; i < Marcador.allMarcs.length;i++){
        if(Marcador.allMarcs[i].id== req.params.id){
             Marcador.allMarcs.splice(i,1);
             res.status(200).json({message:`Marcador de id ${req.params.id} borrado`});
             break
        }
    }
    res.status(400).json({message:`No hay un marcador de id ${req.params.id}`})
}